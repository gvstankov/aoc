#!/bin/env python3
# day5 -- Day 5: Doesn't He Have Intern-Elves For This?
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def solve_part1(input):
    def prop1(str):
        count = 0
        for i in str:
            if i in "aeiou":
                count += 1
        if count >= 3:
            return True
        else:
            return False

    def prop2(str):
        count = 0
        for i in range(len(str)):
            if i + 1 < len(str) and str[i] == str[i+1]:
                count += 1
        if count >= 1:
            return True
        else:
            return False

    def prop3(str):
        if "ab" in str or "cd" in str or "pq" in str or "xy" in str:
            return False
        else:
            return True

    def is_nice(str):
        return prop1(str) and prop2(str) and prop3(str)

    acc = 0
    for line in input:
        if is_nice(line): acc += 1
    print(acc)

def solve_part2(input):
    def prop1(s):
        acc = 0
        for i in range(len(s)):
            if i + 1 < len(s):
                s1 = s[i:i+2]
                s2 = s[i+2:]
                if s2.count(s1) > 0:
                    acc += 1
        if acc >= 1:
            return True
        else:
            return False

    def prop2(str):
        acc = 0
        for i in range(len(str)):
            if i + 2 < len(str) and str[i] == str[i+2]:
                acc += 1
        if acc >= 1:
            return True
        else:
            return False

    def is_nice(str):
        return prop1(str) and prop2(str)

    acc = 0
    for line in input:
        if is_nice(line): acc += 1
    print(acc)

file = open("input.txt", "r")
input = list(map(lambda s: s.rstrip(), file.readlines()))
solve_part1(input)
solve_part2(input)
file.close()
