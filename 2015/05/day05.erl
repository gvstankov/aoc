%% day5 -- Solution to Advent of Code 2015, Day 5: Doesn't He Have Intern-Elves For This?
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day05).
-export([part1/1, part2/1]).

%% Common
split_file(Binary) ->
    string:split(binary_to_list(Binary), "\n", all).

%% Part 1
is_vowel(A) -> lists:member(A, [97, 101, 105, 111, 117]).

vowels([], Acc) ->
    Acc;
vowels([A|Rest], Acc) ->
    vowels(Rest, case is_vowel(A) of
		     true -> Acc + 1;
		     false -> Acc
		 end).
vowels(String) ->
    vowels(String, 0) >= 3.

consecutive([]) -> false;
consecutive([A,B | _]) when A == B -> true;
consecutive([_ | Rest]) -> consecutive(Rest).

ignored([]) -> false;
ignored([A,B | _]) when A == 97 andalso B == 98 -> true;
ignored([A,B | _]) when A == 99 andalso B == 100 -> true;
ignored([A,B | _]) when A == 112 andalso B == 113 -> true;
ignored([A,B | _]) when A == 120 andalso B == 121 -> true;
ignored([_ | Rest]) -> ignored(Rest).

part1_nice(String) ->
    vowels(String)
	and consecutive(String)
	and not ignored(String).

part1(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} -> part1(split_file(Binary), 0)
    end.
part1([], Acc) ->
    Acc;
part1([String | List], Acc) ->
    case part1_nice(String) of
	true ->
	    part1(List, Acc + 1);
	false ->
	    part1(List, Acc)
    end.

%% Part 2
skips([]) -> false;
skips([A,_,B| _]) when A == B -> true;
skips([_| String]) -> skips(String).

twice([]) -> false;
twice([_|[]]) -> false;				% single element at end of list
twice([A,B | Rest]) ->
    case string:find(Rest, [A,B]) of
	nomatch -> twice(lists:append([B], Rest));
	_ -> true
    end.

part2_nice(String) ->
    skips(String) and twice(String).

part2(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} -> part2(split_file(Binary), 0)
    end.
part2([], Acc) ->
    Acc;
part2([String | List], Acc) ->
    case part2_nice(String) of
	true ->
	    part2(List, Acc + 1);
	false ->
	    part2(List, Acc)
    end.
