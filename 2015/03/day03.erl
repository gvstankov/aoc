%% day3 -- Solution to Advent of Code 2015, Day 3: Perfectly Spherical Houses in a Vacuum
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day03).
-export([part1/1, part2/1]).

%% Part 1
part1(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} -> part1(binary_to_list(Binary), [{0,0}]);
	{error, _} -> part1(Filename, [{0,0}])
    end.
part1([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 94 ->  % ^
    part1(Instructions, lists:append([{X - 1, Y}, {X, Y}], Addresses));
part1([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 118 -> % v
    part1(Instructions, lists:append([{X + 1, Y}, {X, Y}], Addresses));
part1([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 62 ->  % >
    part1(Instructions, lists:append([{X, Y + 1}, {X, Y}], Addresses));
part1([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 60 ->  % <
    part1(Instructions, lists:append([{X, Y - 1}, {X, Y}], Addresses));
part1([], Addresses) ->
    length(lists:uniq(Addresses)).

%% Part 2
unindex([{_, A} | B], C) ->
    unindex(B, (lists:append([A], C)));
unindex([], C) ->
    C.
unindex(D) ->
    unindex(D, []).

parse_addr([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 94 ->  % ^
    parse_addr(Instructions, lists:append([{X - 1, Y}, {X, Y}], Addresses));
parse_addr([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 118 -> % v
    parse_addr(Instructions, lists:append([{X + 1, Y}, {X, Y}], Addresses));
parse_addr([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 62 ->  % >
    parse_addr(Instructions, lists:append([{X, Y + 1}, {X, Y}], Addresses));
parse_addr([Instr | Instructions], [{X,Y} | Addresses]) when Instr == 60 ->  % <
    parse_addr(Instructions, lists:append([{X, Y - 1}, {X, Y}], Addresses));
parse_addr([], Addresses) ->
    Addresses.
parse_addr(Instructions) ->
    parse_addr(Instructions, [{0,0}]).

make_rounds({A, B}) ->
    length(
      lists:uniq(
	lists:append(
	  parse_addr(lists:reverse(unindex(A))), % list order matters now
	  parse_addr(lists:reverse(unindex(B)))))).

solve_part2(List) ->
    make_rounds(lists:partition(fun({A, _}) -> A rem 2 == 0 end,
				lists:enumerate(List))).

part2(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} ->
	    solve_part2(binary_to_list(Binary))
    end.
