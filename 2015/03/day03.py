#!/bin/env python3

# day3 -- Day 3: Perfectly Spherical Houses in a Vacuum
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def solve_part1(input):
    houses = [(0,0)]
    for i in input:
        [px, py] = houses[len(houses) - 1]
        match i:
            case '^':
                houses.append((px - 1,py))
            case 'v':
                houses.append((px + 1,py))
            case '>':
                houses.append((px,py + 1))
            case '<':
                houses.append((px,py - 1))
    print(len(set(houses)))

def solve_part2(input):
    santa_houses = [(0,0)]
    robosanta_houses = [(0,0)]

    for i in range(len(input)):
        if i % 2 == 1:
            [spx, spy] = santa_houses[len(santa_houses) - 1]
            match input[i]:
                case '^':
                    santa_houses.append((spx - 1,spy))
                case 'v':
                    santa_houses.append((spx + 1,spy))
                case '>':
                    santa_houses.append((spx,spy + 1))
                case '<':
                    santa_houses.append((spx,spy - 1))
        else:
            [rpx, rpy] = robosanta_houses[len(robosanta_houses) - 1]
            match input[i]:
                case '^':
                    robosanta_houses.append((rpx - 1,rpy))
                case 'v':
                    robosanta_houses.append((rpx + 1,rpy))
                case '>':
                    robosanta_houses.append((rpx,rpy + 1))
                case '<':
                    robosanta_houses.append((rpx,rpy - 1))

    houses = santa_houses + robosanta_houses
    print(len(set(houses)))

file = open("input.txt", "r")
input = file.read()

solve_part1(input)
solve_part2(input)

file.close()
