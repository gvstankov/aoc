%% day7 -- Solution for Advent of Code 2015, Day 7: Some Assembly Required
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day07).
-export([part1/1,part2/1]).

%% Part1
parse_part1([],Wires) -> Wires;
parse_part1([Line|Lines], Wires) ->
    case binary:split(Line, <<" ">>, [global]) of
	%% %%% Assignments
	[Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 47 andalso Byte1 < 96 -> % number
	    parse_part1(Lines, orddict:store(
				 binary_to_atom(Wire), binary_to_integer(Token),
				 Wires));
	[Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 -> % wire
	    case orddict:is_key(binary_to_atom(Token), Wires) of
		true ->
		    parse_part1(Lines,
				orddict:store(
				  binary_to_atom(Wire),
				  orddict:fetch(binary_to_atom(Token), Wires),
				  Wires));
		false ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary NOTs
	[<<"NOT">>, Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 -> % wire
	    case orddict:is_key(binary_to_atom(Token), Wires) of
		true ->
		    parse_part1(Lines,
				orddict:store(
				  binary_to_atom(Wire),
				  256 * 256 - 1 - orddict:fetch(binary_to_atom(Token), Wires), % number
				  Wires));
		false ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary ANDs
	[Token1 = <<Byte1:8,_/binary>>, <<"AND">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 47 andalso Byte1 < 96 andalso Byte2 > 96 -> % Token1 is number, Token2 is wire
	    case orddict:is_key(binary_to_atom(Token2), Wires) of
		true ->
		    parse_part1(Lines, orddict:store(
					 binary_to_atom(Wire),
					 binary_to_integer(Token1) % number
					     band
					     orddict:fetch(binary_to_atom(Token2), Wires), % wire
					 Wires));
		false ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;
	[Token1 = <<Byte1:8,_/binary>>, <<"AND">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 96 -> % Token1 and Token2 are wires
	    case {orddict:is_key(binary_to_atom(Token1), Wires), orddict:is_key(binary_to_atom(Token2), Wires)}  of
		{true, true} ->
		    parse_part1(Lines, orddict:store(
					 binary_to_atom(Wire),
					 orddict:fetch(binary_to_atom(Token1), Wires) % wire
					     band
					     orddict:fetch(binary_to_atom(Token2), Wires), % wire
					 Wires));
		{_,_} ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary ORs
	[Token1 = <<Byte1:8,_/binary>>, <<"OR">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 96 -> % Token1 and Token2 are wires
	    case {orddict:is_key(binary_to_atom(Token1), Wires), orddict:is_key(binary_to_atom(Token2), Wires)}  of
		{true, true} ->
		    parse_part1(Lines, orddict:store
					 (binary_to_atom(Wire),
					  orddict:fetch(binary_to_atom(Token1), Wires) % wire
					      bor
					      orddict:fetch(binary_to_atom(Token2), Wires), % wire
					  Wires));
		{_,_} ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary LSHIFTs
	[Token1 = <<Byte1:8,_/binary>>, <<"LSHIFT">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 47 andalso Byte2 < 96 -> % Token1 is wire, Token2 is number
	    case orddict:is_key(binary_to_atom(Token1), Wires) of
		true ->
		    parse_part1(Lines, orddict:store(
					 binary_to_atom(Wire),
					 orddict:fetch(
					   binary_to_atom(Token1), Wires) % wire
					     bsl
					     binary_to_integer(Token2), % number
					 Wires));
		false ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary RSHIFTs
	[Token1 = <<Byte1:8,_/binary>>, <<"RSHIFT">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 47 andalso Byte2 < 96 -> % Token1 is wire, Token2 is number
	    case orddict:is_key(binary_to_atom(Token1), Wires) of
		true ->
		    parse_part1(Lines,
				orddict:store(binary_to_atom(Wire),
					      orddict:fetch(binary_to_atom(Token1), Wires) % wire
						  bsr
						  binary_to_integer(Token2), % number
					      Wires));
		false ->
		    parse_part1(Lines ++ [Line], Wires)
	    end;

	%% %%% Trailing
	[<<>>] -> parse_part1(Lines, Wires)
    end.

part1(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} -> orddict:fetch(a, parse_part1(
			  binary:split(Binary,
				       [<<"\r\n">>, <<"\n">>],
				       [global]), orddict:new()))
    end.

%% Part 2
parse_part2([],Wires) -> Wires;
parse_part2([Line|Lines], Wires) ->
    case binary:split(Line, <<" ">>, [global]) of
	%% %%% Override
	[_, _, Wire] when Wire == <<"b">> ->
	    parse_part2(Lines, Wires);

	%% %%% Assignments
	[Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 47 andalso Byte1 < 96 -> % number
	    parse_part2(Lines, orddict:store(
				 binary_to_atom(Wire), binary_to_integer(Token),
				 Wires));
	[Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 -> % wire
	    case orddict:is_key(binary_to_atom(Token), Wires) of
		true ->
		    parse_part2(Lines,
				orddict:store(
				  binary_to_atom(Wire),
				  orddict:fetch(binary_to_atom(Token), Wires),
				  Wires));
		false ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary NOTs
	[<<"NOT">>, Token = <<Byte1:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 -> % wire
	    case orddict:is_key(binary_to_atom(Token), Wires) of
		true ->
		    parse_part2(Lines,
				orddict:store(
				  binary_to_atom(Wire),
				  256 * 256 - 1 - orddict:fetch(binary_to_atom(Token), Wires), % number
				  Wires));
		false ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary ANDs
	[Token1 = <<Byte1:8,_/binary>>, <<"AND">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 47 andalso Byte1 < 96 andalso Byte2 > 96 -> % Token1 is number, Token2 is wire
	    case orddict:is_key(binary_to_atom(Token2), Wires) of
		true ->
		    parse_part2(Lines, orddict:store(
					 binary_to_atom(Wire),
					 binary_to_integer(Token1) % number
					     band
					     orddict:fetch(binary_to_atom(Token2), Wires), % wire
					 Wires));
		false ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;
	[Token1 = <<Byte1:8,_/binary>>, <<"AND">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 96 -> % Token1 and Token2 are wires
	    case {orddict:is_key(binary_to_atom(Token1), Wires), orddict:is_key(binary_to_atom(Token2), Wires)}  of
		{true, true} ->
		    parse_part2(Lines, orddict:store(
					 binary_to_atom(Wire),
					 orddict:fetch(binary_to_atom(Token1), Wires) % wire
					     band
					     orddict:fetch(binary_to_atom(Token2), Wires), % wire
					 Wires));
		{_,_} ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary ORs
	[Token1 = <<Byte1:8,_/binary>>, <<"OR">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 96 -> % Token1 and Token2 are wires
	    case {orddict:is_key(binary_to_atom(Token1), Wires), orddict:is_key(binary_to_atom(Token2), Wires)}  of
		{true, true} ->
		    parse_part2(Lines, orddict:store
					 (binary_to_atom(Wire),
					  orddict:fetch(binary_to_atom(Token1), Wires) % wire
					      bor
					      orddict:fetch(binary_to_atom(Token2), Wires), % wire
					  Wires));
		{_,_} ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary LSHIFTs
	[Token1 = <<Byte1:8,_/binary>>, <<"LSHIFT">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 47 andalso Byte2 < 96 -> % Token1 is wire, Token2 is number
	    case orddict:is_key(binary_to_atom(Token1), Wires) of
		true ->
		    parse_part2(Lines, orddict:store(
					 binary_to_atom(Wire),
					 orddict:fetch(
					   binary_to_atom(Token1), Wires) % wire
					     bsl
					     binary_to_integer(Token2), % number
					 Wires));
		false ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Binary RSHIFTs
	[Token1 = <<Byte1:8,_/binary>>, <<"RSHIFT">>, Token2 = <<Byte2:8,_/binary>>, <<"->">>, Wire] when Byte1 > 96 andalso Byte2 > 47 andalso Byte2 < 96 -> % Token1 is wire, Token2 is number
	    case orddict:is_key(binary_to_atom(Token1), Wires) of
		true ->
		    parse_part2(Lines,
				orddict:store(binary_to_atom(Wire),
					      orddict:fetch(binary_to_atom(Token1), Wires) % wire
						  bsr
						  binary_to_integer(Token2), % number
					      Wires));
		false ->
		    parse_part2(Lines ++ [Line], Wires)
	    end;

	%% %%% Trailing
	[<<>>] -> parse_part2(Lines, Wires)
    end.

part2(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} ->
	    OldWires = parse_part1(
			 binary:split(Binary,
				      [<<"\r\n">>, <<"\n">>],
				      [global]),
			 orddict:new()),
	    orddict:fetch(a, parse_part2(
			       binary:split(Binary,
					    [<<"\r\n">>, <<"\n">>],
					    [global]),
			       orddict:store(b,
					     orddict:fetch(a, OldWires),
					     orddict:new())))
    end.
