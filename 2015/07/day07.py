#!/bin/env python3
# day6 -- Day 7: Some Assembly Required
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from collections import deque

def wire_or_signal(i):
    try:
        int(i)
    except:
        return i, "wire"
    else:
        return int(i), "num"

def solve_part1(input):
    wires = dict()
    queue = deque(input)
    while len(queue) > 0:
        line = queue.popleft()
        spl = line.split()
        match len(spl):
            case 3:
                a1 = wire_or_signal(spl[0])
                a2 = wire_or_signal(spl[2])
                if a1[1] == "num":
                    wires.update({a2[0]: a1[0]})
                elif a1[0] in wires:
                    wires.update({a2[0]: wires.get(a1[0])})
                else:
                    queue.append(line)
            case 4:
                a1 = wire_or_signal(spl[1])
                a2 = wire_or_signal(spl[3])
                match spl[0]:
                    case 'NOT':
                        if a1[1] == "num":
                            wires.update({a2[0]: 256 * 256 - 1 - a1[0]})
                        elif a1[0] in wires:
                            wires.update({a2[0]: 256 * 256 - 1 - wires.get(a1[0])})
                        else:
                            queue.append(line)
            case 5:
                a1 = wire_or_signal(spl[0])
                a2 = wire_or_signal(spl[2])
                a3 = wire_or_signal(spl[4])
                match spl[1]:
                    case 'AND':
                        if a1[1] == "num" and a2[1] == "num":
                            wires.update({a3[0]: a1[0] & a2[0]})
                        elif a1[0] in wires and a2[1] == "num":
                            wires.update({a3[0]: wires.get(a1[0]) & a2[0]})
                        elif a2[0] in wires and a1[1] == "num":
                            wires.update({a3[0]: wires.get(a2[0]) & a1[0]})
                        elif a1[0] in wires and a2[0] in wires:
                            wires.update({a3[0]: wires.get(a2[0]) & wires.get(a1[0])})
                        else:
                            queue.append(line)
                    case 'OR':
                        if a1[1] == "num" and a2[1] == "num":
                            wires.update({a3[0]: a1[0] | a2[0]})
                        elif a1[0] in wires and a2[1] == "num":
                            wires.update({a3[0]: wires.get(a1[0]) | a2[0]})
                        elif a2[0] in wires and a1[1] == "num":
                            wires.update({a3[0]: a1[0] | wires.get(a2[0])})
                        elif a1[0] in wires and a2[0] in wires:
                            wires.update({a3[0]: wires.get(a2[0]) | wires.get(a1[0])})
                        else:
                            queue.append(line)
                    case 'LSHIFT':
                        if a1[1] == "num" and a2[1] == "num":
                            wires.update({a3[0]: a1[0] << a2[0]})
                        elif a1[0] in wires and a2[1] == "num":
                            wires.update({a3[0]: wires.get(a1[0]) << a2[0]})
                        elif a2[0] in wires and a1[1] == "num":
                            wires.update({a3[0]: a1[0] << wires.get(a2[0])})
                        elif a1[0] in wires and a2[0] in wires:
                            wires.update({a3[0]: wires.get(a1[0]) << wires.get(a2[0])})
                        else:
                            queue.append(line)
                    case 'RSHIFT':
                        if a1[1] == "num" and a2[1] == "num":
                            wires.update({a3[0]: a2[0] >> a1[0]})
                        elif a1[0] in wires and a2[1] == "num":
                            wires.update({a3[0]: wires.get(a1[0]) >> a2[0]})
                        elif a2[0] in wires and a2[1] == "num":
                            wires.update({a3[0]: a1[0] >> wires.get(a2[0])})
                        elif a1[0] in wires and a2[0] in wires:
                            wires.update({a3[0]: wires.get(a1[0]) >> wires.get(a2[0])})
                        else:
                            queue.append(line)
    return wires.get('a')

def solve_part2(input):
    wires = dict({'b': solve_part1(input)})
    queue = deque(input)
    while len(queue) > 0:
        line = queue.popleft()
        spl = line.split()
        match len(spl):
            case 3:
                a1 = wire_or_signal(spl[0])
                a2 = wire_or_signal(spl[2])
                if a2[0] == 'b':
                    pass
                else:
                    if a1[1] == "num":
                        wires.update({a2[0]: a1[0]})
                    elif a1[0] in wires:
                        wires.update({a2[0]: wires.get(a1[0])})
                    else:
                        queue.append(line)
            case 4:
                a1 = wire_or_signal(spl[1])
                a2 = wire_or_signal(spl[3])
                match spl[0]:
                    case 'NOT':
                        if a2[0] == 'b':
                            pass
                        else:
                            if a1[1] == "num":
                                wires.update({a2[0]: 256 * 256 - 1 - a1[0]})
                            elif a1[0] in wires:
                                wires.update({a2[0]: 256 * 256 - 1 - wires.get(a1[0])})
                            else:
                                queue.append(line)
            case 5:
                a1 = wire_or_signal(spl[0])
                a2 = wire_or_signal(spl[2])
                a3 = wire_or_signal(spl[4])
                match spl[1]:
                    case 'AND':
                        if a3[0] == 'b':
                            pass
                        else:
                            if a1[1] == "num" and a2[1] == "num":
                                wires.update({a3[0]: a1[0] & a2[0]})
                            elif a1[0] in wires and a2[1] == "num":
                                wires.update({a3[0]: wires.get(a1[0]) & a2[0]})
                            elif a2[0] in wires and a1[1] == "num":
                                wires.update({a3[0]: wires.get(a2[0]) & a1[0]})
                            elif a1[0] in wires and a2[0] in wires:
                                wires.update({a3[0]: wires.get(a2[0]) & wires.get(a1[0])})
                            else:
                                queue.append(line)
                    case 'OR':
                        if a3[0] == 'b':
                            pass
                        else:
                            if a1[1] == "num" and a2[1] == "num":
                                wires.update({a3[0]: a1[0] | a2[0]})
                            elif a1[0] in wires and a2[1] == "num":
                                wires.update({a3[0]: wires.get(a1[0]) | a2[0]})
                            elif a2[0] in wires and a1[1] == "num":
                                wires.update({a3[0]: a1[0] | wires.get(a2[0])})
                            elif a1[0] in wires and a2[0] in wires:
                                wires.update({a3[0]: wires.get(a2[0]) | wires.get(a1[0])})
                            else:
                                queue.append(line)
                    case 'LSHIFT':
                        if a3[0] == 'b':
                            pass
                        else:
                            if a1[1] == "num" and a2[1] == "num":
                                wires.update({a3[0]: a1[0] << a2[0]})
                            elif a1[0] in wires and a2[1] == "num":
                                wires.update({a3[0]: wires.get(a1[0]) << a2[0]})
                            elif a2[0] in wires and a1[1] == "num":
                                wires.update({a3[0]: a1[0] << wires.get(a2[0])})
                            elif a1[0] in wires and a2[0] in wires:
                                wires.update({a3[0]: wires.get(a1[0]) << wires.get(a2[0])})
                            else:
                                queue.append(line)
                    case 'RSHIFT':
                        if a3[0] == 'b':
                            pass
                        else:
                            if a1[1] == "num" and a2[1] == "num":
                                wires.update({a3[0]: a2[0] >> a1[0]})
                            elif a1[0] in wires and a2[1] == "num":
                                wires.update({a3[0]: wires.get(a1[0]) >> a2[0]})
                            elif a2[0] in wires and a2[1] == "num":
                                wires.update({a3[0]: a1[0] >> wires.get(a2[0])})
                            elif a1[0] in wires and a2[0] in wires:
                                wires.update({a3[0]: wires.get(a1[0]) >> wires.get(a2[0])})
                            else:
                                queue.append(line)
    return wires.get('a')

file = open("input.txt", "r")
input = list(map(lambda s: s.rstrip(), file.readlines()))
file.close()
print(solve_part1(input))
print(solve_part2(input))
