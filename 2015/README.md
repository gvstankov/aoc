# Advent of Code 2015 Solutions

My attempts at solving all [Advent of Code 2015][aoc2015] puzzles using
multiple languages.

## Example: solve a single puzzle

Download your Advent of Code 2015 Day 1 puzzle input and save it as
`path_to_this_repo/2015/01/input.txt`.

Examples assume working directory is `2015/01`:

```console
$ cd path_to_this_repo/2015/01/
```

### Erlang

```console
$ erl
1> c("day01.erl").
{ok,day1}
2> day1:part1("input.txt").
1234
3> day1:part2("input.txt").
5678
```

### Python
```console
$ ./day01.py
1234
5678
```

### Awk
```console
$ gawk -f day01.awk input.txt
1234
5678
```

### C/C++
```console
$ cc -o day01 day01.c && ./day01
1234
5678
```

### Object Pascal
```console
$ fpc day01.pas && ./day01
1234
5678
```

## Environment

Developed and tested on:

### Erlang

```console
$ erl -version
Erlang (SMP,ASYNC_THREADS) (BEAM) emulator version 14.2.5
```

### Python

```console
$ python3 --version
Python 3.10.12
```

### Awk

```console
$ gawk --version
GNU Awk 5.1.0, API: 3.0 (GNU MPFR 4.1.0, GNU MP 6.2.1)
Copyright (C) 1989, 1991-2020 Free Software Foundation.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see http://www.gnu.org/licenses/.
```

### C/C++
```console
$ cc --version
cc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
$ gcc --version
gcc (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
$ g++ --version
g++ (Ubuntu 11.4.0-1ubuntu1~22.04) 11.4.0
Copyright (C) 2021 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```

### Object Pascal

```console
$ fpc -iV
3.2.2
```


[aoc2015]: https://adventofcode.com/2015/

<!--
Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
-->
