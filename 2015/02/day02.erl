%% day2 -- Solution to Advent of Code 2015, Day 2: I Was Told There Would Be No Math
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day02).
-export([part1/1, part2/1]).

%% Common
split_file(Binary) ->
    string:split(binary_to_list(Binary), "\n", all).

parse_present(Present) ->
    [{list_to_integer(lists:nth(1, string:split(Present, "x", all))),
      list_to_integer(lists:nth(2, string:split(Present, "x", all))),
      list_to_integer(lists:nth(3, string:split(Present, "x", all)))}].

parse_presents_to_tuples([[]], List) ->
    List;
parse_presents_to_tuples([Present | Presents], List) ->
    parse_presents_to_tuples(Presents,
			     lists:append(parse_present(Present), List)).

%% Part 1
smallest_surface({Length, Width, Height}) ->
    lists:min([Length * Width, Length * Height, Width * Height]).

present_surface({Length, Width, Height}) ->
    2 * Length * Width +
	2 * Length * Height +
	2 * Width * Height +
	smallest_surface({Length, Width, Height}).

all_surfaces([], List) ->
    List;
all_surfaces([Present | Presents], List) ->
    all_surfaces(Presents, lists:append([present_surface(Present)], List)).

solve_part1(Presents) ->
    lists:sum(all_surfaces(parse_presents_to_tuples(Presents, []), [])).

part1(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} ->
	    solve_part1(split_file(Binary))
    end.

%% Part 2
present_ribbon_lenght({Length, Width, Height}) ->
    lists:min([2 * Length + 2 * Width,
	       2 * Length + 2 * Height,
	       2 * Width + 2 * Height])
	+ Length * Width * Height.

all_lenghts([], List) ->
    List;
all_lenghts([Present | Presents], List) ->
    all_lenghts(Presents, lists:append([present_ribbon_lenght(Present)], List)).

solve_part2(Presents) ->
    lists:sum(all_lenghts(parse_presents_to_tuples(Presents, []), [])).

part2(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} ->
	    solve_part2(split_file(Binary))
    end.
