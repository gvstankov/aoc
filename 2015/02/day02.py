#!/bin/env python3

# day2 -- Solution to Advent of Code 2015, Day 2: I Was Told There Would Be No Math
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def strip_newline(str):
    return str.rstrip()

def cubic_surface_area(l,w,h):
    return 2*l*w + 2*w*h + 2*h*l

def cubic_volume(l,w,h):
    return l*w*h

def smallest_side_area(l, w, h):
    return min([l*w, l*h, w*h])

def smallest_side_perimeter(l, w, h):
    return min([2*l + 2*w, 2*l + 2*h, 2*w + 2*h])

def solve_part1(input):
    paper_ft2 = 0
    for i in input:
        [l,w,h] = list(map(int, i.split('x')))
        paper_ft2 += cubic_surface_area(l,w,h) + smallest_side_area(l,w,h)
    print(paper_ft2)

def solve_part2(input):
    ribbon_len = 0
    for i in input:
        [l,w,h] = list(map(int, i.split('x')))
        ribbon_len += smallest_side_perimeter(l,w,h) + cubic_volume(l,w,h)
    print(ribbon_len)

file = open("input.txt", "r")
input = list(map(strip_newline, file.readlines()))

solve_part1(input)
solve_part2(input)

file.close()
