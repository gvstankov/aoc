// day1 -- Solution to Advent of Code 2015, Day 1: Not Quite Lisp
// Copyright (C) 2024  Georgi Stankov

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <fstream>
#include <string>

void part1() {
  std::string line;
  std::ifstream file("input.txt");

  if (file.is_open()) {
    int Stack = 0;
    while (getline(file, line)) {
      for (int i = 0; i < line.size(); i++) {
	if (line[i] == '(') {
	  Stack += 1;
	} else if (line[i] == ')') {
	  Stack -= 1;
	}
      }
      std::cout << Stack << "\n";
    }

    file.close();
  }
}

void part2() {
  std::string line;
  std::ifstream file("input.txt");

  if (file.is_open()) {
    int Stack = 0;
    while (getline(file, line)) {
      for (int i = 0; i < line.size(); i++) {
	if (line[i] == '(') {
	  Stack += 1;
	} else if (line[i] == ')') {
	  Stack -= 1;
	}
	if (Stack == -1) {
	  std::cout << i + 1 << "\n";
	  break;
	}
      }
    }

    file.close();
  }
}

int main() {
  part1();
  part2();
  return 0;
}
