#!/bin/env python3

# day1 -- Solution to Advent of Code 2015, Day 1: Not Quite Lisp
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def solve_part1(instrs):
    stack = 0
    for instr in instrs:
        if instr == "(":
            stack += 1
        elif instr == ")":
            stack -= 1
    print(stack)

def solve_part2(instrs):
    stack = 0
    for instr in range(len(instrs)):
        if instrs[instr] == "(":
            stack += 1
        elif instrs[instr] == ")":
            stack -= 1

        if stack == -1:
            print(instr + 1)    # instructions are 1-indexed, add 1
            break

file = open("input.txt", "r")
input = file.read()

solve_part1(input)
solve_part2(input)

file.close()
