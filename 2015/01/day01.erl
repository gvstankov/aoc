%% day1 -- Solution to Advent of Code 2015, Day 1: Not Quite Lisp
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day01).
-export([part1/1, part2/1]).

%% Part 1
solve_part1([Inst | Instructions], Stack) when [Inst] =:= "(" ->
    solve_part1(Instructions, Stack + 1);
solve_part1([Inst | Instructions], Stack) when [Inst] =:= ")" ->
    solve_part1(Instructions, Stack - 1);
solve_part1([], Stack) ->
    Stack.
solve_part1(Instructions) ->
    solve_part1(Instructions, 0).

part1(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} ->
	    solve_part1(binary_to_list(Binary))
    end.

%% Part 2
solve_part2([{Index, _}| _], Stack) when Stack == -1 ->
    Index - 1;					% stack was already -1
solve_part2([{_, Inst} | Instructions], Stack) when Inst == 40 ->
    solve_part2(Instructions, Stack + 1);
solve_part2([{_, Inst} | Instructions], Stack) when Inst == 41 ->
    solve_part2(Instructions, Stack - 1).
solve_part2(Instructions) ->
    solve_part2(Instructions, 0).

part2(Filename) ->
    case file:read_file(Filename) of
	{ok, Binary} ->
	    solve_part2(lists:enumerate(binary_to_list(Binary)))
    end.
