/* day1 -- summary */
/* Copyright (C) 2024  Georgi Stankov */

/* This program is free software: you can redistribute it and/or modify */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or */
/* (at your option) any later version. */

/* This program is distributed in the hope that it will be useful, */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the */
/* GNU General Public License for more details. */

/* You should have received a copy of the GNU General Public License */
/* along with this program.  If not, see <https://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *slurp_file_to_string(char *file_path) {
  FILE *fp = fopen(file_path, "r");
  if (fp == NULL) {
    fprintf(stderr, "Cannot open %s\n", file_path);
    exit(EXIT_FAILURE);
  };
  fseek(fp, 0L, SEEK_END);
  size_t fp_sz = ftell(fp) + 1;
  rewind(fp);
  char input[fp_sz];
  fgets(input, fp_sz, fp);
  fclose(fp);
  char *result = malloc((fp_sz + 1)*sizeof(char));
  result = input;
  return result;
}

int solve_part1(char *input){
  int floor = 0;
  for (unsigned int i = 0; i < strlen(input); i++) {
    (input[i] == '(') ? floor++ : floor--;
  }
  return floor;
}

unsigned int solve_part2(char *input){
  int floor = 0;
  unsigned int pos;
  for (pos = 0; pos < strlen(input); pos++) {
    if (floor == -1) { break; } else {
      (input[pos] == '(') ? floor++ : floor--;
    }
  }
  return pos;
}

int main(){
  char *input = slurp_file_to_string("input.txt");
  printf("%d\n", solve_part1(input));
  printf("%d\n", solve_part2(input));
}
