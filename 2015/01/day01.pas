// day -- summary
// Copyright (C) 2024  Georgi Stankov

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

program day1;

{$mode objfpc}

uses
   SysUtils;

const
   C_FNAME = 'input.txt';

var
   tfIn: TextFile;
   s0: AnsiString;
   s1: AnsiString;
   s2: AnsiString;
   stack1: Integer = 0;
   stack2: Integer = 0;
   c1: Char;
   c2: Char;

begin
   // Set the name of the file that will be read
   AssignFile(tfIn, C_FNAME);

   // Embed the file handling in a try/except block to handle errors gracefully
   try

      // Open the file for reading
      reset(tfIn);

      // Keep reading lines until the end of the file is reached
      while not eof(tfIn) do
      begin
	 readln(tfIn, s0);
      end;

      // Done so close the file
      CloseFile(tfIn);

   except
      on E: EInOutError do
	 writeln('File handling error occurred. Details: ', E.Message);
   end;

   s1 := s0;

   // Part1
   for c1 in s1 do
   begin
      if c1 = '(' then
	 stack1 := stack1 + 1
      else
	 stack1 := stack1 - 1
   end;
   writeln(stack1);
end.

// Local Variables:
// mode: opascal
// End:
