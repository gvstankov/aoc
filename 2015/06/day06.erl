%% day6 -- Solution to Advent of Code 2015, Day 6: Probably a Fire Hazard
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day06).
-export([part1/1,part2/1]).

%% Part 1
part1_parse_instr([], Grid) ->
    gb_sets:size(Grid);
part1_parse_instr([Instruction|Rest], Grid) ->
    part1_parse_instr(Rest,
		      case binary:split(Instruction, <<" ">>, [global]) of
			  [<<"turn">>, <<"on">>, Begin, _, End] ->
			      [X1, Y1] = binary:split(Begin, <<",">>),
			      [X2, Y2] = binary:split(End, <<",">>),
			      gb_sets:union(Grid, gb_sets:from_list([{X,Y} ||
									X <- lists:seq(binary_to_integer(X1),
										       binary_to_integer(X2)),
									Y <- lists:seq(binary_to_integer(Y1),
										       binary_to_integer(Y2))]));
			  [<<"turn">>, <<"off">>, Begin, _, End] ->
			      [X1, Y1] = binary:split(Begin, <<",">>),
			      [X2, Y2] = binary:split(End, <<",">>),
			      gb_sets:subtract(Grid, gb_sets:from_list([{X,Y} ||
									   X <- lists:seq(binary_to_integer(X1),
											  binary_to_integer(X2)),
									   Y <- lists:seq(binary_to_integer(Y1),
											  binary_to_integer(Y2))]));
			  [<<"toggle">>, Begin, _, End] ->
			      [X1, Y1] = binary:split(Begin, <<",">>),
			      [X2, Y2] = binary:split(End, <<",">>),
			      Todo = gb_sets:from_list([{X,Y} ||
							   X <- lists:seq(binary_to_integer(X1),
									  binary_to_integer(X2)),
							   Y <- lists:seq(binary_to_integer(Y1),
									  binary_to_integer(Y2))]),
			      Missing = gb_sets:difference(Todo, Grid),
			      Surplus = gb_sets:intersection(Todo, Grid),
			      gb_sets:union(gb_sets:subtract(Grid, Surplus), Missing);
			  [<<>>] -> Grid
		      end).

part1(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} -> part1_parse_instr(binary:split(Binary, [<<"\r\n">>, <<"\n">>], [global]),
					  gb_sets:new())
    end.

%% Part 2
brightness(Grid) ->
    brightness(Grid, [I || I <- lists:seq(1, 1000000)], 0).
brightness(_, [], Acc) ->
    Acc;
brightness(Grid, [Ind|Indexes], Acc) ->
    brightness(Grid, Indexes, Acc + counters:get(Grid, Ind)).

part2_parse_instr([],Grid) ->
    brightness(Grid);
part2_parse_instr([Instruction|Rest],Grid) ->
    case binary:split(Instruction, <<" ">>, [global]) of
	[<<"turn">>, <<"on">>, Begin, _, End] ->
	    [X1, Y1] = binary:split(Begin, <<",">>),
	    [X2, Y2] = binary:split(End, <<",">>),
	    lists:foreach(fun (X) ->
				  counters:add(Grid, X + 1 ,1) % counters indexes are 1-based
			  end,
			  [Index || Index <-
					lists:map(fun ({A,B}) -> A * 1000 + B end,
						  [{X,Y} ||
						      X <- lists:seq(binary_to_integer(X1),
								     binary_to_integer(X2)),
						      Y <- lists:seq(binary_to_integer(Y1),
								     binary_to_integer(Y2))])]);
	[<<"turn">>, <<"off">>, Begin, _, End] ->
	    [X1, Y1] = binary:split(Begin, <<",">>),
	    [X2, Y2] = binary:split(End, <<",">>),
	    lists:foreach(fun (X) ->
				  case counters:get(Grid, X + 1) of % counters indexes are 1-based
				      0 -> ok;
				      _ -> counters:sub(Grid, X + 1,1) % counters indexes are 1-based
				  end
			  end,
			  [Index || Index <-
					lists:map(fun ({A,B}) -> A * 1000 + B end,
						  [{X,Y} ||
						      X <- lists:seq(binary_to_integer(X1),
								     binary_to_integer(X2)),
						      Y <- lists:seq(binary_to_integer(Y1),
								     binary_to_integer(Y2))])]);
	[<<"toggle">>, Begin, _, End] ->
	    [X1, Y1] = binary:split(Begin, <<",">>),
	    [X2, Y2] = binary:split(End, <<",">>),
	    lists:foreach(fun (X) ->
				  counters:add(Grid, X + 1, 2) % counters indexes are 1-based
			  end,
			  [Index || Index <-
					lists:map(fun ({A,B}) -> A * 1000 + B end,
						  [{X,Y} ||
						      X <- lists:seq(binary_to_integer(X1),
								     binary_to_integer(X2)),
						      Y <- lists:seq(binary_to_integer(Y1),
								     binary_to_integer(Y2))])]);
	[<<>>] -> Grid
    end,
    part2_parse_instr(Rest, Grid).
part2(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} -> part2_parse_instr(binary:split(Binary, [<<"\r\n">>, <<"\n">>], [global]),
					  counters:new(1000 * 1000, [atomics]))
    end.
