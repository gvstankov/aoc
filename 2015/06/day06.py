#!/bin/env python3
# day6 -- Day 6: Probably a Fire Hazard
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

def range_to_index_list(x0, y0, x1, y1):
    return list(map(lambda a: a[0]*1000 + a[1],
                    ((x,y)
                     for x in
                     [x for x in range(x0, x1+1)]
                     for y in
                     [y for y in range(y0, y1+1)])))

def solve_part1(input):
    lights = [0 for x in range(1_000_000)]

    for line in input:
        spl=line.split()
        match spl[0]:
            case 'turn':
                match spl[1]:
                    case 'on':
                        x0, y0 = list(map(int, spl[2].split(',')))
                        x1, y1 = list(map(int, spl[4].split(',')))
                        for i in range_to_index_list(x0, y0, x1, y1):
                            lights[i] = 1
                    case 'off':
                        x0, y0 = list(map(int, spl[2].split(',')))
                        x1, y1 = list(map(int, spl[4].split(',')))
                        for i in range_to_index_list(x0, y0, x1, y1):
                            lights[i] = 0
            case 'toggle':
                x0, y0 = list(map(int, spl[1].split(',')))
                x1, y1 = list(map(int, spl[3].split(',')))
                for i in range_to_index_list(x0, y0, x1, y1):
                    if lights[i] == 1:
                        lights[i] = 0
                    elif lights[i] == 0:
                        lights[i] = 1

    print(lights.count(1))

def solve_part2(input):
    lights = [0 for x in range(1_000_000)]

    for line in input:
        spl=line.split()
        match spl[0]:
            case 'turn':
                match spl[1]:
                    case 'on':
                        x0, y0 = list(map(int, spl[2].split(',')))
                        x1, y1 = list(map(int, spl[4].split(',')))
                        for i in range_to_index_list(x0, y0, x1, y1):
                            lights[i] += 1
                    case 'off':
                        x0, y0 = list(map(int, spl[2].split(',')))
                        x1, y1 = list(map(int, spl[4].split(',')))
                        for i in range_to_index_list(x0, y0, x1, y1):
                            if lights[i] == 0:
                                pass
                            else:
                                lights[i] -= 1
            case 'toggle':
                x0, y0 = list(map(int, spl[1].split(',')))
                x1, y1 = list(map(int, spl[3].split(',')))
                for i in range_to_index_list(x0, y0, x1, y1):
                    lights[i] += 2

    print(sum(lights))

file = open("input.txt", "r")
input = list(map(lambda s: s.rstrip(), file.readlines()))
file.close()
solve_part1(input)
solve_part2(input)
