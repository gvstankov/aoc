%% day4 -- Solution to Advent of Code 2015, Day 4: The Ideal Stocking Stuffer
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day04).
-export([part1/1, part2/1]).

%% Common
encode_hash(String) ->
    binary:encode_hex(crypto:hash(md5, String)).
encode_hash(String, Int) ->
    encode_hash(lists:append(String, integer_to_list(Int))).

%% Part 1
part1(_, Hash, Int) when binary_part(Hash, 0, 5) == <<"00000">> ->
    Int;
part1(InitStr, _, Int) ->
    part1(InitStr, encode_hash(InitStr, Int + 1), Int + 1).

part1(Hash) ->
    part1(Hash, <<>>, 1).

%% Part 2
part2(_, Hash, Int) when binary_part(Hash, 0, 6) == <<"000000">> ->
    Int;
part2(InitStr, _, Int) ->
    part2(InitStr, encode_hash(InitStr, Int + 1), Int + 1).

part2(Hash) ->
    part2(Hash, <<>>, 1).
