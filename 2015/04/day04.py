#!/bin/env python3
# day4 -- Day 4: The Ideal Stocking Stuffer
# Copyright (C) 2024  Georgi Stankov

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from hashlib import md5
from sys import maxsize

def solve_part1(input):
    for i in range(1, maxsize):
        hashable = input + str(i)
        hash = md5(hashable.encode()).hexdigest()
        if hash[0:5] == "00000":
            print(i)
            break

def solve_part2(input):
    for i in range(1, maxsize):
        hashable = input + str(i)
        hash = md5(hashable.encode()).hexdigest()
        if hash[0:6] == "000000":
            print(i)
            break

file = open("input.txt", "r")
input = file.read()

solve_part1(input)
solve_part2(input)

file.close()
