%% day8 -- Solution to Advent of Code 2015, Day 8: Matchsticks
%% Copyright (C) 2024  Georgi Stankov

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

-module(day08).
-export([part1/1, part2/1]).

%% Part 1
decode_string(<<>>, StringCounter, CharCounter) ->		% return result
    {StringCounter, CharCounter};
decode_string(<<A,Rest/binary>>, StringCounter, CharCounter) when A == 34 ->
    decode_string(Rest, StringCounter + 1, CharCounter);
decode_string(<<A,B,Rest/binary>>, StringCounter, CharCounter) when
      A == 92 andalso B /= 120 -> % escaped char
    decode_string(Rest, StringCounter + 2, CharCounter + 1);
decode_string(<<A,B,C,D,Rest/binary>>, StringCounter, CharCounter) when
      A == 92 andalso B == 120 -> % escaped hex?
    try binary:decode_hex(<<C,D>>)
    catch error:badarg ->			% no
	    decode_string(<<C,D,Rest/binary>>,StringCounter + 2, CharCounter + 1)
    end,
    decode_string(Rest, StringCounter + 4, CharCounter + 1);	% yes
decode_string(<<_,Rest/binary>>, StringCounter, CharCounter) ->	% normal char
    decode_string(Rest, StringCounter + 1, CharCounter + 1).

parse_part1([<<>>], StrAcc, CharAcc) ->
    StrAcc - CharAcc;
parse_part1([Line|Rest], StrAcc, CharAcc) ->
    {Str, Char} = decode_string(Line, 0, 0),
    parse_part1(Rest, Str + StrAcc, Char + CharAcc).

part1(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} -> parse_part1(binary:split(Binary,
				       [<<"\r\n">>, <<"\n">>],
				       [global]), 0, 0)
    end.

%% Part 2
encode_string(<<A,Rest/binary>>, StringCounter, CharCounter) when
      A == 34 orelse A == 92 ->			% quote or backslash
    encode_string(Rest, StringCounter + 1, CharCounter + 2);
encode_string(<<_,Rest/binary>>, StringCounter, CharCounter) -> % any char
    encode_string(Rest, StringCounter + 1, CharCounter + 1);
encode_string(<<>>, StringCounter, CharCounter) ->
    {StringCounter, CharCounter + 2}.		% surrounding quotes

parse_part2([<<>>], StrAcc, CharAcc) ->
    CharAcc - StrAcc;
parse_part2([Line|Rest], StrAcc, CharAcc) ->
    {Str, Char} = encode_string(Line, 0, 0),
    parse_part2(Rest, Str + StrAcc, Char + CharAcc).

part2(Arg) ->
    case file:read_file(Arg) of
	{ok, Binary} -> parse_part2(binary:split(Binary,
				       [<<"\r\n">>, <<"\n">>],
				       [global]), 0, 0)
    end.
