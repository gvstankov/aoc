# Advent of Code solutions

My attempts at solving all [Advent of Code][aoc] puzzles.

**NOT** affiliated with Advent of Code.

The [AoC][aoc] puzzles are designed to be simple enough to be solvable within a
day and that makes them an accessible way to familiarize yourself with different
programming languages and paradigms by solving the same small and self-contained
problems in different ways.

## Goals

1. Self-contained solutions to both parts of the puzzle
1. No external dependencies, only built-ins and standard library (if available)
1. Exploring parts of standard libraries that I do not use often or at all
1. Having "fun"
1. Learn to code good (and learn to do other stuff good too)

## Non-Goals

1. Fast and efficient solutions
1. Solving the puzzles in the intended time limit

### Nice-to-Haves

1. Multiple solutions using different approaches (same language)
1. Per-solution commentary
1. Well-written documentation

[aoc]: https://adventofcode.com/

<!--
Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.
-->
